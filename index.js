var express = require('./node_modules/express');
var app = express();
var path = require('path');

app.use(express.static(__dirname + '/'));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname + '/greycod/index.html'));
});



app.listen(8080);
// http://localhost:8080
